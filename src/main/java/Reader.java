import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

public class Reader {

	private static Map<String, Integer> groups = new HashMap<>();

	public static void main(String[] args) throws IOException {
		if (args.length != 1) {
			throw new RuntimeException("Please speficy the path to SPELL.BIN as first argument");
		}

		Path path = Paths.get(args[0]);
		byte[] bytes = Files.readAllBytes(path);

		{
			JSONObject binary = new JSONObject();
			binary.put("data", toHex(Arrays.copyOfRange(bytes, 0, 8), false));
			binary.put("name", "header");
			JSONArray groupIds = new JSONArray();
			groupIds.put(createGroupIfNotExists("Header"));
			binary.put("groupIds", groupIds);
			binary = post("http://localhost:8080/hexanalyzer/rest/binary", binary);
		}

		// Skip header
		for (int position = 8; position < 52648;) {
			int length;
			String type;
			if (bytes[position] == 0x01) {
				type = "Spell";
				if (position == 0x3DAC) {
					length = 328;
				} else if (position == 0x4678 || position == 0x47C4) {
					length = 332;
				} else if (position == 0x4910) {
					length = 368;
				} else if (position == 0x5440) {
					length = 344;
				} else if (position == 0x74C4) {
					length = 336;
				} else if (position == 0x8010) {
					length = 328;
				} else if (position == 0xCAC4) {
					length = 776;
				} else {
					length = 324;
				}
			} else if (bytes[position] == 0x04) {
				type = "Effect";
				if (position == 0x1434 || position == 0x72E0 || position == 0x9BCC || position == 0x9D9C
						|| position == 0xAB98) {
					length = 160;
				} else if (position == 0x2780 || position == 0xA0A0) {
					length = 164;
				} else if (position == 0x76AC || position == 0xA004) {
					length = 156;
				} else {
					length = 152;
				}
			} else if (bytes[position] == 0x07) {
				type = "Shield";
				if (position == 0x5048) {
					length = 152;
				} else {
					length = 144;
				}
			} else if (bytes[position] == 0x09) {
				type = "Gate";
				length = 104;
			} else if (bytes[position] == 0x08) {
				type = "Dismissal";
				if (position == 0xCA64) {
					length = 96;
				} else {
					length = 100;
				}
			} else if (bytes[position] == 0x02) {
				type = "Wall";
				length = 144;
			} else if (bytes[position] == 0x0A) {
				type = "Rune";
				length = 288;
			} else {
				throw new RuntimeException(
						"Unknown type " + bytes[position] + " as position " + String.format("%08X ", position));
			}

			byte[] spell = Arrays.copyOfRange(bytes, position, position + length);

			String name = getName(spell, 4);

			// debug(spell, position, length, special);

			JSONObject binary = new JSONObject();
			binary.put("data", toHex(spell, false));
			binary.put("name", type.toLowerCase() + "_" + cleanup(name));
			JSONArray groupIds = new JSONArray();
			groupIds.put(createGroupIfNotExists(type));
			binary.put("groupIds", groupIds);
			binary = post("http://localhost:8080/hexanalyzer/rest/binary", binary);

			if (type.equals("Spell")) {
				if (spell[0x80] == (byte) 0xFF && spell[0x81] == (byte) 0xFF && spell[0x82] == (byte) 0xFF
						&& spell[0x83] == (byte) 0xFF) {
					post("http://localhost:8080/hexanalyzer/rest/analysis", createAnalysis("grey", "all_ff",
							"Always FFFFFFFF for spells", 0x80, 0x83, binary.getInt("id")));
				}

				if (spell[0x0100] == (byte) 0x80 && spell[0x0101] == (byte) 0x02 && spell[0x0102] == (byte) 0x00
						&& spell[0x0103] == (byte) 0x00) {
					post("http://localhost:8080/hexanalyzer/rest/analysis", createAnalysis("grey", "all_8002",
							"Always 80020000 for spells", 0x0100, 0x0103, binary.getInt("id")));
				}
			}

			if (spell[0x0024] == (byte) 0x00 && spell[0x0025] == (byte) 0x00 && spell[0x0026] == (byte) 0x00
					&& spell[0x0027] == (byte) 0x00) {
				post("http://localhost:8080/hexanalyzer/rest/analysis", createAnalysis("grey", "all_00_1",
						"Always 00000000 for everything", 0x0024, 0x0027, binary.getInt("id")));
			}

			if (spell[0x002C] == (byte) 0x2F && spell[0x002D] == (byte) 0x00 && spell[0x002E] == (byte) 0x00
					&& spell[0x002F] == (byte) 0x00) {
				post("http://localhost:8080/hexanalyzer/rest/analysis", createAnalysis("blue", "healer_spell",
						"2F000000 for healer spells", 0x002C, 0x002F, binary.getInt("id")));
			} else {
				post("http://localhost:8080/hexanalyzer/rest/analysis", createAnalysis("grey", "healer_spell",
						"00000000 for non-healer spells", 0x002C, 0x002F, binary.getInt("id")));
			}

			post("http://localhost:8080/hexanalyzer/rest/analysis", createAnalysis("grey", "all_00_2",
					"Always 00000000 for everything", 0x0030, 0x0047, binary.getInt("id")));

			if (type.equals("Rune")) {
				post("http://localhost:8080/hexanalyzer/rest/analysis", createAnalysis("orange", "rune_5C",
						"Always 50000000 for runes", 0x005C, 0x005F, binary.getInt("id")));
			} else if (type.equals("Spell")) {
				post("http://localhost:8080/hexanalyzer/rest/analysis", createAnalysis("orange", "spell_5C",
						"Always 64000000 for spells", 0x005C, 0x005F, binary.getInt("id")));
			} else if (type.equals("Dismissal")) {
				post("http://localhost:8080/hexanalyzer/rest/analysis", createAnalysis("orange", "dismissal_5C",
						"Always 80020000 for dismissals", 0x005C, 0x005F, binary.getInt("id")));
			} else if (type.equals("Effect")) {
				post("http://localhost:8080/hexanalyzer/rest/analysis", createAnalysis("orange", "effect_5C",
						"From 00000000 to 30000000 for effects", 0x005C, 0x005F, binary.getInt("id")));
			}

			if (spell[0x0028] != (byte) 0x00 || spell[0x0029] != (byte) 0x00 || spell[0x002A] != (byte) 0x00
					|| spell[0x002B] != (byte) 0x00) {
				post("http://localhost:8080/hexanalyzer/rest/analysis",
						createAnalysis("cyan", "spell_category",
								"Group of the spells (spells with the same group are likely upgrades of each other)",
								0x0028, 0x002B, binary.getInt("id")));
			}

			post("http://localhost:8080/hexanalyzer/rest/analysis",
					createAnalysis("yellow", "type", type, 0, 3, binary.getInt("id")));
			post("http://localhost:8080/hexanalyzer/rest/analysis",
					createAnalysis("red", "name", name, 4, 23, binary.getInt("id")));
			post("http://localhost:8080/hexanalyzer/rest/analysis", createAnalysis("green", "minumum_level",
					"Minumum level of this " + type, 0x9C, 0x9F, binary.getInt("id")));

			position += length;
		}

		Integer lawGroup = createGroupIfNotExists("Laws");

		for (int position = 52684; position < bytes.length;) {
			byte[] law = Arrays.copyOfRange(bytes, position, position + 176);

			String name = getName(law, 0);
			if (name.length() != 0) {
				JSONObject binary = new JSONObject();
				binary.put("data", toHex(law, false));
				binary.put("name", "law_" + cleanup(name));
				JSONArray groupIds = new JSONArray();
				groupIds.put(lawGroup);
				binary.put("groupIds", groupIds);
				binary = post("http://localhost:8080/hexanalyzer/rest/binary", binary);

				post("http://localhost:8080/hexanalyzer/rest/analysis",
						createAnalysis("red", "name", "Name of this law", 0, 19, binary.getInt("id")));
				post("http://localhost:8080/hexanalyzer/rest/analysis", createAnalysis("grey", "all_same_law_1",
						"Always the same for laws", 0x14, 0x37, binary.getInt("id")));
				post("http://localhost:8080/hexanalyzer/rest/analysis", createAnalysis("grey", "all_same_law_2",
						"Always the same for laws", 0x98, 0xAF, binary.getInt("id")));
			}
			if (position == 0xEECC) {
				break;
			}
			position += 176;

		}

	}

	private static Integer createGroupIfNotExists(String name)
			throws MalformedURLException, IOException, ProtocolException {
		Integer groupId = groups.get(name);
		if (groupId == null) {
			JSONObject group = new JSONObject();
			group.put("name", name);
			group = post("http://localhost:8080/hexanalyzer/rest/group", group);
			groupId = group.getInt("id");
			groups.put(name, groupId);
		}
		return groupId;
	}

	private static JSONObject post(String uri, JSONObject binary)
			throws MalformedURLException, IOException, ProtocolException {
		URL url = new URL(uri);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);
		conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
		String encoded = Base64.getEncoder().encodeToString("admin:admin".getBytes(StandardCharsets.UTF_8));
		conn.setRequestProperty("Authorization", "Basic " + encoded);
		try (OutputStream os = conn.getOutputStream()) {
			os.write(binary.toString().getBytes(StandardCharsets.UTF_8));
		}
		try (InputStream in = conn.getInputStream(); Scanner scanner = new Scanner(in, "UTF-8")) {
			String inputStreamString = scanner.useDelimiter("\\A").next();
			return new JSONObject(inputStreamString);
		}
	}

	private static JSONObject createAnalysis(String color, String name, String description, int start, int end,
			int binaryId) {
		JSONObject analysisEntry = new JSONObject();
		analysisEntry.put("color", color);
		analysisEntry.put("name", name);
		analysisEntry.put("description", description);
		analysisEntry.put("start", Integer.toString(start, 16));
		analysisEntry.put("end", Integer.toString(end, 16));
		analysisEntry.put("binaryId", binaryId);
		return analysisEntry;
	}

	private static String cleanup(String name) {
		return name.toLowerCase().replace(' ', '_');
	}

	private static String getName(byte[] spell, int position) {
		byte[] arr = Arrays.copyOfRange(spell, position, position + 20);
		int i;
		for (i = 0; i < arr.length && arr[i] != 0; i++) {
		}
		return new String(arr, 0, i, StandardCharsets.ISO_8859_1);
	}

	private static StringBuilder toHex(byte[] bytes, boolean beautify) {
		String format = beautify ? "%02X " : "%02X";
		StringBuilder sb = new StringBuilder();
		int count = 0;
		for (byte b : bytes) {
			sb.append(String.format(format, b));
			if (beautify && (count % 16 == 15)) {
				sb.append('\n');
			}
			count++;
		}
		return sb;
	}
}
