## [1.0.2](https://gitlab.com/appframework/egore-personal/spell-bin-reader/compare/1.0.1...1.0.2) (2025-02-26)

## [1.0.1](https://gitlab.com/appframework/egore-personal/spell-bin-reader/compare/1.0.0...1.0.1) (2025-01-15)


### Bug Fixes

* **deps:** update dependency org.json:json to v20250107 ([5ef4fe5](https://gitlab.com/appframework/egore-personal/spell-bin-reader/commit/5ef4fe5b5de23b56e91084909a15bfccfc401ae6))

# 1.0.0 (2025-01-09)


### Bug Fixes

* **deps:** update dependency org.json:json to v20241224 ([934acaa](https://gitlab.com/appframework/egore-personal/spell-bin-reader/commit/934acaafe4ce93c3b87013a762d5c974562a3d69))
